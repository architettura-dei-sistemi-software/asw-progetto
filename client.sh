#!/bin/bash

GET_OUTPUT=$(curl -X GET localhost:8080 2>&1)

until [[ $GET_OUTPUT =~ :.\[.*\] ]]
do
	GET_OUTPUT=$(curl -X GET localhost:8080 2>&1)
	echo 'Servizio non ancora disponibile, eseguo un nuovo tentativo.'
	sleep 2
done

sleep 5

echo 'Servizio disponibile!'

for i in {1..5} 
do
	curl -s -X POST localhost:8080 
	echo 'POST inviata'
    sleep 1
done

curl -X GET localhost:8080
echo ""

for i in {1..5} 
do
	curl -s -X POST localhost:8080 
	echo 'POST inviata'
    sleep 1
done

curl -X GET localhost:8080
echo ""
