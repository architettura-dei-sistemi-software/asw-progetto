package it.gomc.servicec.model;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ServiceC {
//	@Value("${service.names}")
//    private String[] names;
//	@Value("${service.index}")
//    private int i;
	@Value("${it.gomc.servicec.name}")
	private String name;
	
	private ArrayList<String> lista = new ArrayList<String>();
	
	public void aggiungiAvvistamento(String request)
	{
		System.out.println("Richiesta ricevuta: " + request);
		lista.add(request);
	}

	public String getAvvistamenti() {
		String res = name + ": ";
		res += lista.toString();
		return res;
	}
}
