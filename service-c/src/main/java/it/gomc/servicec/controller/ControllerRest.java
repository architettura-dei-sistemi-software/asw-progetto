package it.gomc.servicec.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.gomc.servicec.ServiceCApplication;
import it.gomc.servicec.model.ServiceC;;

@RestController
public class ControllerRest {
	
	@Autowired
	private ServiceC serviceC;	
	
	@PostMapping("/")
	public String postRequest(@RequestBody String request)
	{
		serviceC.aggiungiAvvistamento(request);
		return request;		
	}
	
	@GetMapping("/")
	public String getRequest()
	{			
		return serviceC.getAvvistamenti();
	}
	
	@Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping("/service-instances/{applicationName}")
    public List<ServiceInstance> serviceInstancesByApplicationName(
            @PathVariable String applicationName) {
        return this.discoveryClient.getInstances(applicationName);
    }
	
	private static Logger log = LoggerFactory.getLogger(ServiceCApplication.class);

	  @RequestMapping(value = "/greeting")
	  public String greet() {
	    log.info("Access /greeting");

	    List<String> greetings = Arrays.asList("Hi there", "Greetings", "Salutations");
	    Random rand = new Random();

	    int randomNum = rand.nextInt(greetings.size());
	    return greetings.get(randomNum);
	  }
}
