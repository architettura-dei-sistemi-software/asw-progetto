package it.gomc.serviceb;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("serviceC")
public interface FeignServiceC {
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String getRequest();
	
	@PostMapping("/")
	public String postRequest(String request);
}
