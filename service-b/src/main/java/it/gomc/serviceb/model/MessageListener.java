package it.gomc.serviceb.model;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class MessageListener {
	@Value("${it.gomc.serviceb.channel.in}")
	private String channel;
	
	@Value("${it.gomc.serviceb.groupId}")
	private String groupId;
	
	@Autowired
	private ServizioB servizioB;
	
	@KafkaListener(topics="${it.gomc.serviceb.channel.in}",
					groupId="${it.gomc.serviceb.groupId}")
	public void listen(ConsumerRecord<String, String> record) {
		String message = record.value();
		System.out.println("Messaggio ricevuto: " + message);
		servizioB.onMessage(message);
	}
}
