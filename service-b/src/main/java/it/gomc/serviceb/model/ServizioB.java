package it.gomc.serviceb.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.gomc.serviceb.FeignServiceC;

@Service
public class ServizioB {
//	@Value("${service.names}")
//    private String[] names;
//	@Value("${service.index}")
//    private int i;
	@Value("${it.gomc.serviceb.name}")
	private String name;
	
	@Autowired
	private FeignServiceC serviceC;
	
	public void onMessage(String message) {
		String avvistamento = name + ": " + message;
		serviceC.postRequest(avvistamento);
	}
}
