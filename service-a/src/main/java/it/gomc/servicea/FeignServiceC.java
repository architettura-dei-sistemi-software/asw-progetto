package it.gomc.servicea;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("serviceC")
public interface FeignServiceC {
	
	@GetMapping(value="/")
	public String getRequest();
	
	//@PostMapping("/")
	//public String postRequest(String request);
}
