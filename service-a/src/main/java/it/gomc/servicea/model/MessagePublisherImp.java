package it.gomc.servicea.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessagePublisherImp implements MessagePublisher {
	
	@Value("${it.gomc.servicea.channel.out}")
	private String channel;
	
	@Autowired
	private KafkaTemplate<String, String> template;
	
	@Override
	public void publish(String message) {
		template.send(channel, message);
	}

}
