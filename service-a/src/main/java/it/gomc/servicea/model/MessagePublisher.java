package it.gomc.servicea.model;

public interface MessagePublisher{
	public void publish(String message);
}
