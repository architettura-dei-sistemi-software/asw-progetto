package it.gomc.servicea.model;

import java.util.Random;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ServiceAModel {
	
//	@Value("${service.names}")
//    private String[] names;
//	@Value("${service.index}")
//    private int i;
	@Value("${it.gomc.servicea.name}")
	private String name;
	
	@Value("${it.gomc.servicea.animali}")
	private String[] animali;
	
	public String avvistaAnimale()
	{
		return name + ": " + animali[( (new Random()).nextInt(animali.length) )];
	}

}
