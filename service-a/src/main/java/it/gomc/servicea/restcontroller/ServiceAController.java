package it.gomc.servicea.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import it.gomc.servicea.FeignServiceC;
import it.gomc.servicea.model.MessagePublisherImp;
import it.gomc.servicea.model.ServiceAModel;

@RestController
public class ServiceAController {
	
	@Autowired
	private FeignServiceC serviceC;
	
	@Autowired
	private ServiceAModel serviceA;
	
	@Autowired
	private MessagePublisherImp publisher;

	@Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    RestTemplate restTemplate;

	@PostMapping("/")
	public void POSTRequest() {
		String avvistamento = serviceA.avvistaAnimale();
//		serviceC.postRequest(avvistamento);
		publisher.publish(avvistamento);
	}

	@GetMapping("/")
	public String GETRequest() {
		return serviceC.getRequest();
	}

	/**
	 * Controllo collegamento tramite Eureka
	 * 
	 * @param applicationName
	 * @return
	 */
    @RequestMapping("/service-instances/{applicationName}")
    public List<ServiceInstance> serviceInstancesByApplicationName(
            @PathVariable String applicationName) {
        return this.discoveryClient.getInstances(applicationName);
    }
    
    @LoadBalanced
    @Bean
    RestTemplate restTemplate(){
      return new RestTemplate();
    }

//    @RequestMapping("/hi")
//    public String hi(@RequestParam(value="name", defaultValue="Artaban") String name) {
//      String greeting = this.restTemplate.getForObject("http://serviceC/greeting", String.class);
//      return String.format("%s, %s!", greeting, name);
//    }
}
