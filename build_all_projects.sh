#!/bin/bash

#cd service-a && ./gradlew build && ./gradlew build docker && cd ../service-b && ./gradlew build && ./gradlew build docker && cd ../service-c && ./gradlew build && ./gradlew build docker && cd ../service-zuul && ./gradlew build && ./gradlew build docker && cd ../service-eureka && ./gradlew build && ./gradlew build docker && cd ..
echo "Building all projects..."
echo ""
PROJECTS=$(ls | grep 'service-*')
for i in ${PROJECTS}
do
    echo "Building project ${i}"
    echo ""
    cd ${i}
    rm -r build
    rm -r bin
    ./gradlew build
    ./gradlew build docker
    cd ..
    echo "Built project ${i}"
    echo ""
done
